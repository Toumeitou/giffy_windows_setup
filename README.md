# Setting up Giffy on Windows
## Set up Git
- Download Git from [here](https://msysgit.github.io/)
- Right click the .exe and choose `Run as Administrator`
- Leave everything at defaults except in the screen "Adjusting your PATH environment" check "Use Git from the Windows Command prompt" [see here](http://i.imgur.com/rTTVIxS.png)
- Create the folder `C:\code` - everything we'll do will reside here
## Set up Python and virtualenv
- Download Python 2.7 from [here](https://www.python.org/downloads/)
- Set the install path to `C:\code\Python27`
- In "Customize Python 2.7.x" set "Add python.exe to Path" to "Will be installed on local hard drive" like [this](http://i.imgur.com/CHs30eh.png)
- Wait for the install to complete
- Open your command prompt (press the Windows key, type "cmd" and press Enter)
- Type `pip install virtualenv`
## Set up server project
### PostgreSQL
- Download PostgreSQL from [here](http://www.enterprisedb.com/products-services-training/pgdownload#windows)
- Set the install directory to `C:\code\postgresql`, leave rest at defaults
- Add `C:\code\postgresql\bin` to your PATH variable
### Clone project
- Open the Git bash
- `cd` to `/c/code/`
- `git clone https://bitbucket.org/mihails_tumkins/giffy.git`
### Install dependencies
- Download and install [this](http://aka.ms/vcpython27)
- Download [this](http://downloads.sourceforge.net/gnuwin32/file-5.03-setup.exe) and install it to `C:\code\filetests`
- Add `C:\code\filetests\bin` to your PATH variable
- Download [this](http://www.stickpeople.com/projects/python/win-psycopg/2.6.1/psycopg2-2.6.1.win32-py2.7-pg9.4.4-release.exe) and copy it `C:\code\giffy`. Rename it to `psycopg2.exe`.
- Open your command prompt
- `cd /D C:\code\giffy`
- `virtualenv env`
- `env\Scripts\activate`
- `easy_install psycopg2.exe`
- `pip install -r conf/requirements.txt`
### Start
- `python giffy.py`
- Test by browsing to `localhost:8080`
## Set up AndroidStudio
- Download Java Development Kit from [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- Install it in `C:/code/JDK`
- Download AndroidStudio from [here](http://developer.android.com/sdk/index.html)
- If the installer asks for JDK location browse to `C:\code\JDK`
- Install AndroidStudio and Android SDK in [easy to remember locations](http://i.imgur.com/HunDCeH.png) (e.g. `C:\code\AndroidStudio` and `C:\code\android-sdk`)
- Leave the rest at defaults
- Start up AndroidStudio. In setup wizard switch theme to [Dracula](http://i.imgur.com/xgdSdsO.png), leave rest at defaults.
## Set up Android client
- Open the Git bash
- `cd` to `/c/code`
- `git clone https://bitbucket.org/Toumeitou/giffy_android.git`
- Open Android Studio
- Choose "Open existing" and browse to `C:\code\giffy_android`
### If you want to use the emulator
- In `C:\code\android-sdk` open `SDK Manager`
- Check and download "Intel x86 Emulator Accelerator (XAHM installer)"
- Go to `C:\code\android-sdk\extras\intel\Hardware_Accelerated_Execution_Manager` and install `intelhaxm-android.exe`
- You might have to enable "Hardware Virtualization" in your computer's BIOS